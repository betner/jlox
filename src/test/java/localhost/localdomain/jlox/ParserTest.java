package localhost.localdomain.jlox;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ParserTest {

    @Test
    void parseEmptyText() {
        var code = "";
        var tokens = new Scanner(code).tokens();
        var expression = new Parser(tokens).parse();

        assertTrue(expression.isEmpty());
    }

    @Test
    void parseValidExpression() {
        var code = "3 * (-2 + 4);";
        var tokens = new Scanner(code).tokens();
        var expression = new Parser(tokens).parse();

        assertNotNull(expression);
    }


    @Test
    void parseInvalidExpression() {
        var code = "+3 * /();";
        var tokens = new Scanner(code).tokens();

        assertThrows(ParseError.class, ()-> new Parser(tokens).parse());
    }
}