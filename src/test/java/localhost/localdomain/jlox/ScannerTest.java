package localhost.localdomain.jlox;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;

import static localhost.localdomain.jlox.Token.Type.*;

class ScannerTest {

  @Test
  void operators() {
    var scanner = new Scanner("!*+-/=<> <= >= ==");
    var expectedTypes = List.of(BANG, STAR, PLUS, MINUS, FRONT_SLASH, EQUAL, LESS, GREATER, LESS_EQUAL, GREATER_EQUAL,
        EQUAL_EQUAL, EOF);
    var tokenTypes = scanner.tokens().stream().map(Token::type).toList();

    Assertions.assertEquals(expectedTypes, tokenTypes);
  }

  @Test
  void whitespace() {
    var scanner = new Scanner("\n \r \t   ");
    var expectedTypes = List.of(EOF);
    var tokenTypes = scanner.tokens().stream().map(Token::type).toList();

    Assertions.assertEquals(expectedTypes, tokenTypes);
  }

  @Test
  void lineComment() {
    var scanner = new Scanner("! // *!/ \"comment\"");
    var expectedTypes = List.of(BANG, EOF);
    var tokenTypes = scanner.tokens().stream().map(Token::type).toList();

    Assertions.assertEquals(expectedTypes, tokenTypes);
  }

  @Test
  void blockComment() {
    var scanner = new Scanner(
        """
            identifier
            /*
             * A block comment should be ignored
             * and no tokens (!*<+-) parsed
             */
             ==
            """);
    var expectedTypes = List.of(IDENTIFIER, EQUAL_EQUAL, EOF);
    var tokenTypes = scanner.tokens().stream().map(Token::type).toList();

    Assertions.assertEquals(expectedTypes, tokenTypes);
  }

  @Test
  void unexpectedSymbol() {
    var scanner = new Scanner("!@");
    var expectedTypes = List.of(BANG, EOF);
    var tokenTypes = scanner.tokens().stream().map(Token::type).toList();

    Assertions.assertEquals(expectedTypes, tokenTypes);
  }

  @Test
  void string() {
    var scanner = new Scanner("\"a string\"");
    var expectedTypes = List.of(STRING, EOF);
    var tokenTypes = scanner.tokens().stream().map(Token::type).toList();

    Assertions.assertEquals(expectedTypes, tokenTypes);
  }

  @Test
  void multiLineString() {
    var scanner = new Scanner("\"a \nstring\"");
    var expectedTypes = List.of(STRING, EOF);
    var tokenTypes = scanner.tokens().stream().map(Token::type).toList();

    Assertions.assertEquals(expectedTypes, tokenTypes);
  }

  @Test
  void unterminatedString() {
    var scanner = new Scanner("\"a string");
    var expectedTypes = List.of(EOF);
    var tokenTypes = scanner.tokens().stream().map(Token::type).toList();

    Assertions.assertEquals(expectedTypes, tokenTypes);
  }

  @Test
  void number() {
    var scanner = new Scanner("1 1.0 0.1 11.23 000.2");
    var expectedTypes = List.of(NUMBER, NUMBER, NUMBER, NUMBER, NUMBER, EOF);
    var tokenTypes = scanner.tokens().stream().map(Token::type).toList();

    Assertions.assertEquals(expectedTypes, tokenTypes);
  }

  @Test
  void numberWithDotsInWrongPlace() {
    var scanner = new Scanner(".123 123.");
    var expectedTypes = List.of(DOT, NUMBER, NUMBER, DOT, EOF);
    var tokenTypes = scanner.tokens().stream().map(Token::type).toList();

    Assertions.assertEquals(expectedTypes, tokenTypes);
  }

  @Test
  void identifier() {
    var scanner = new Scanner("aa Aa zebra Zebra _id i_d id_ c0de");
    var expectedTypes = List.of(IDENTIFIER, IDENTIFIER, IDENTIFIER, IDENTIFIER, IDENTIFIER, IDENTIFIER, IDENTIFIER,
        IDENTIFIER, EOF);
    var tokenTypes = scanner.tokens().stream().map(Token::type).toList();

    Assertions.assertEquals(expectedTypes, tokenTypes);
  }

  @Test
  void keywords() {
    // print is a built-in function so the name counts as an IDENTIFIER
    var scanner = new Scanner("and class else false for fun if nil or print return super this true var while");
    var expectedTypes = List.of(AND, CLASS, ELSE, FALSE, FOR, FUN, IF, NIL, OR, IDENTIFIER, RETURN, SUPER, THIS,
            TRUE, VAR, WHILE, EOF);
    var tokenTypes = scanner.tokens().stream().map(Token::type).toList();

    Assertions.assertEquals(expectedTypes, tokenTypes);
  }

  @Test
  void grouping() {
    var scanner = new Scanner("{  } ( )");
    var expectedTypes = List.of(LEFT_BRACE, RIGHT_BRACE, LEFT_PAREN, RIGHT_PAREN, EOF);
    var tokenTypes = scanner.tokens().stream().map(Token::type).toList();

    Assertions.assertEquals(expectedTypes, tokenTypes);
  }

  @Test
  void punctuation() {
    var scanner = new Scanner(". , ;");
    var expectedTypes = List.of(DOT, COMMA, SEMICOLON, EOF);
    var tokenTypes = scanner.tokens().stream().map(Token::type).toList();

    Assertions.assertEquals(expectedTypes, tokenTypes);
  }
}
