package localhost.localdomain.jlox;

import org.junit.jupiter.api.Test;

import static localhost.localdomain.jlox.Token.Type.PLUS;

class ExpressionTest {

    @Test
    void createExpression() {
        Expression expr = new Expression.Binary(new Expression.Literal(3), new Token(PLUS, "+", null, 1), new Expression.Literal(5));
        System.out.println(expr);
    }
}