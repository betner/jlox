package localhost.localdomain.jlox;

import java.util.*;

/**
 * Static analyzer that resolves variables and implements Lexical scoping.
 * Unused local variables are considered errors.
 * The Resolver also makes sure that return statement are not legal outside a scope.
 */
public class Resolver implements Statement.Visitor<Void>, Expression.Visitor<Void> {

    // the state of the local variable, defined or undefined
    public static final Boolean UNDEFINED = Boolean.FALSE;
    public static final Boolean DEFINED = Boolean.TRUE;

    private final Interpreter interpreter;
    private final Stack<Map<String, Boolean>> scopes = new Stack<>();
    private final Map<String, Token> unresolvedLocals = new HashMap<>();
    private FunctionType currentFunction = FunctionType.NONE;
    private ClassType currentClass = ClassType.NONE;

    public Resolver(Interpreter interpreter) {
        this.interpreter = interpreter;
    }

    @Override
    public Void visit(Expression.Binary binary) {
        resolve(binary.left());
        resolve(binary.right());
        return null;
    }

    @Override
    public Void visit(Expression.Unary unary) {
        resolve(unary.right());
        return null;
    }

    @Override
    public Void visit(Expression.Grouping grouping) {
        resolve(grouping.expression());
        return null;
    }

    @Override
    public Void visit(Expression.Literal literal) {
        return null;
    }

    @Override
    public Void visit(Expression.Variable variable) {
        // check if the variable used has been defined yet
        // this solves the case:
        // -- var a;
        // -- a = a;
        if (!scopes.isEmpty() &&
            scopes.peek().get(variable.name().lexeme()) == UNDEFINED) {
            Lox.error(variable.name(), "Cannot read local variable in its own initializer.");
        }
        resolveLocal(variable, variable.name());

        return null;
    }

    @Override
    public Void visit(Expression.Assignment assignment) {
        resolve(assignment.value());
        resolveLocal(assignment, assignment.name());
        return null;
    }

    @Override
    public Void visit(Expression.Logical logical) {
        resolve(logical.left());
        resolve(logical.right());
        return null;
    }

    @Override
    public Void visit(Expression.Call call) {
        resolve(call.callee());
        for (Expression argument : call.arguments()) {
            resolve(argument);
        }

        return null;
    }

    @Override
    public Void visit(Expression.Get get) {
        // properties are resolved dynamically
        // do nothing just continue recurse
        resolve(get.object());
        return null;
    }

    @Override
    public Void visit(Expression.Set set) {
        // properties are resolved dynamically
        // do nothing just continue recurse into the subexpressions
        resolve(set.value());
        resolve(set.object());
        return null;
    }

    @Override
    public Void visit(Statement.Class statement) {
        var enclosingClass = currentClass;
        currentClass = ClassType.CLASS;

        declare(statement.name());
        define(statement.name());

        beginScope();
        scopes.peek().put("this", true);

        for (Statement.Function method : statement.methods()) {
            FunctionType type = FunctionType.METHOD; // default to regular method
            if (method.name().lexeme().equals("init")) {
                type = FunctionType.INITIALIZER;
            }
            resolveFunction(method, type);
        }
        endScope();

        currentClass = enclosingClass;
        return null;
    }

    @Override
    public Void visit(Expression.This expr) {
        if (currentClass == ClassType.NONE) {
            Lox.error(expr.keyword(),
                    "Cannot use 'this' outside of a class.");
            return null;
        }

        // handle like a variable with the name "this"
        resolveLocal(expr, expr.keyword());
        return null;
    }

    @Override
    public Void visit(Statement.Function statement) {
        declare(statement.name());
        define(statement.name());
        resolveFunction(statement, FunctionType.FUNCTION);

        return null;
    }

    @Override
    public Void visit(Statement.Var statement) {
        declare(statement.name());
        if (statement.initializer() != null) {
            resolve(statement.initializer());
        }
        define(statement.name());

        // local variable, track it to see if it's unused
        if (!scopes.isEmpty()) {
            var globallyUniqueName = statement.name().lexeme() + scopes.size();
            unresolvedLocals.put(globallyUniqueName, statement.name());
        }

        return null;
    }

    @Override
    public Void visit(Statement.Expr statement) {
        resolve(statement.expression());
        return null;
    }

    @Override
    public Void visit(Statement.If statement) {
        resolve(statement.condition());
        resolve(statement.thenBranch());
        if (statement.elseBranch() != null) resolve(statement.elseBranch());

        return null;
    }

    @Override
    public Void visit(Statement.While statement) {
        resolve(statement.condition());
        resolve(statement.body());
        return null;
    }

    @Override
    public Void visit(Statement.Block statement) {
        beginScope();
        resolve(statement.statements());
        endScope();

        return null;
    }

    @Override
    public Void visit(Statement.Return statement) {
        if (currentFunction == FunctionType.NONE) {
            Lox.error(statement.keyword(), "Cannot return from top-level code, must be done inside a scope.");
        }

        if (currentFunction == FunctionType.INITIALIZER) {
            Lox.error(statement.keyword(), "Cannot return from a class initializer.");
        }

        if (statement.value() != null) resolve(statement.value());
        return null;
    }

    void run(List<Statement> program) {
       resolve(program);
       if (!unresolvedLocals.isEmpty()) {
           var unused = unresolvedLocals.values().stream().toList().getFirst();
           Lox.error(unused, "Local variable is not used.");
       }
    }

    private void beginScope() {
        scopes.push(new HashMap<>());
    }

    private void endScope() {
       scopes.pop();
    }

    private void declare(Token name) {
        if (scopes.isEmpty()) return;
        Map<String, Boolean> scope = scopes.peek();

        if (scope.containsKey(name.lexeme())) {
            Lox.error(name, "There is already a variable declared with this name in this scope.");
        }

        scope.put(name.lexeme(), UNDEFINED);
    }

    private void define(Token name) {
        if (scopes.isEmpty()) return;

        scopes.peek().put(name.lexeme(), DEFINED);
    }

    private void resolve(List<Statement> statements) {
       statements.forEach(this::resolve);
    }

    private void resolve(Statement statement) {
       statement.accept(this);
    }

    private void resolve(Expression expression) {
        expression.accept(this);
    }

    private void resolveLocal(Expression expression, Token name) {
        var depth = 0;
        for (int i = scopes.size() - 1; i >= 0; i--) {
            if (scopes.get(i).containsKey(name.lexeme())) {
                depth = scopes.size() - 1 - i;
                interpreter.resolve(expression, depth); // found in this scope (0), next outer is 1 etc.

                var globallyUniqueName = name.lexeme() + depth; // the unique name for the variable in the nearest scope it was found in
                unresolvedLocals.remove(globallyUniqueName); // the local variable is used
                return;
            }
        }
    }

    private void resolveFunction(Statement.Function function, FunctionType type) {
        var enclosingFunction = currentFunction;
        currentFunction = type;

        beginScope();
        for (Token parameter : function.parameters()) {
            declare(parameter);
            define(parameter);
        }
        resolve(function.body());
        endScope();

        currentFunction = enclosingFunction;
    }

    public enum FunctionType {
        FUNCTION, INITIALIZER, METHOD, NONE
    }

    public enum ClassType {
        NONE, CLASS, SUBCLASS
    }
}
