package localhost.localdomain.jlox;

import java.util.List;

/**
 * A Statement in the AST
 */
public interface Statement {

   <T> T accept(Visitor<T> visitor);

    interface Visitor<T> {
        T visit(Class statement);
        T visit(Function statement);
        T visit(Var statement);
        T visit(Expr statement);
        T visit(If statement);
        T visit(While statement);
        T visit(Block statement);
        T visit(Return statement);
    }

    record Class(Token name, List<Statement.Function> methods) implements Statement {
        @Override
        public <T> T accept(Visitor<T> visitor) {
            return visitor.visit(this);
        }
    }

    record Function(Token name, List<Token> parameters, List<Statement> body) implements Statement {
        @Override
        public <T> T accept(Visitor<T> visitor) {
            return visitor.visit(this);
        }
    }

    record Var(Token name, Expression initializer) implements Statement {
        @Override
        public <T> T accept(Visitor<T> visitor) {
            return visitor.visit(this);
        }

    }
    record Expr(Expression expression) implements Statement {

        @Override
        public <T> T accept(Visitor<T> visitor) {
            return visitor.visit(this);
        }

    }
    record If(Expression condition, Statement thenBranch, Statement elseBranch) implements Statement {
        @Override
        public <T> T accept(Visitor<T> visitor) {
            return visitor.visit(this);
        }

    }
    record While(Expression condition, Statement body) implements Statement {
        @Override
        public <T> T accept(Visitor<T> visitor) {
            return visitor.visit(this);
        }

    }
    record Block(List<Statement> statements) implements Statement {
        @Override
        public <T> T accept(Visitor<T> visitor) {
            return visitor.visit(this);
        }

    }

    record Return(Token keyword, Expression value) implements Statement {
        @Override
        public <T> T accept(Visitor<T> visitor) {
            return visitor.visit(this);
        }
    }
}
