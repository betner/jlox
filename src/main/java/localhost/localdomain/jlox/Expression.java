package localhost.localdomain.jlox;

import java.util.List;

/**
 * An Expression in the AST
 */
interface Expression {
    // Visitor pattern
    <T> T accept(Visitor<T> visitor);

    interface Visitor<T> {
        T visit(Binary binary);
        T visit(Unary unary);
        T visit(Grouping grouping);
        T visit(Literal literal);
        T visit(Variable variable);
        T visit(Assignment assignment);
        T visit(Logical logical);
        T visit(Call call);
        T visit(Get get);
        T visit(Set set);
        T visit(This expr);
    }

    // Expression type classes, calls correct method in the visitor
    record Binary(Expression left, Token operator, Expression right) implements Expression {
        @Override
        public <T> T accept(Visitor<T> visitor) { return visitor.visit(this); }
    }

    record Unary(Token operator, Expression right) implements Expression {
        @Override
        public <T> T accept(Visitor<T> visitor) { return visitor.visit(this); }
    }

    record Grouping(Expression expression) implements Expression {
        @Override
        public <T> T accept(Visitor<T> visitor) { return visitor.visit(this); }
    }

    record Literal(Object value) implements Expression {
        @Override
        public <T> T accept(Visitor<T> visitor) { return visitor.visit(this); }
    }

    record Variable(Token name) implements Expression {
        @Override
        public <T> T accept(Visitor<T> visitor) {
            return visitor.visit(this);
        }
    }

    record Assignment(Token name, Expression value) implements Expression {
        @Override
        public <T> T accept(Visitor<T> visitor) {
            return visitor.visit(this);
        }
    }

    record Logical(Expression left, Token operator, Expression right) implements Expression {
        @Override
        public <T> T accept(Visitor<T> visitor) {
            return visitor.visit(this);
        }
    }

    record Call(Expression callee, Token rightParen, List<Expression> arguments) implements Expression {
        @Override
        public <T> T accept(Visitor<T> visitor) {
            return visitor.visit(this);
        }
    }

    record Get(Expression object, Token name) implements Expression {
        @Override
        public <T> T accept(Visitor<T> visitor) {
            return visitor.visit(this);
        }
    }

    record Set(Expression object, Token name, Expression value) implements Expression {
        @Override
        public <T> T accept(Visitor<T> visitor) {
            return visitor.visit(this);
        }
    }

    record This(Token keyword) implements Expression {
        @Override
        public <T> T accept(Visitor<T> visitor) {
            return visitor.visit(this);
        }
    }
}
