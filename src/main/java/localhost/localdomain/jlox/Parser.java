package localhost.localdomain.jlox;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

import static localhost.localdomain.jlox.Token.Type.*;

/**
 * Single token lookahead recursive descent parser with no backtracking
 */
public class Parser {

    private final List<Token> tokens;
    private int current = 0;

    Parser(List<Token> tokens) {
        this.tokens = tokens;
    }

    List<Statement> parse() {
       var statements = new ArrayList<Statement>();

       while (!isAtEnd()) {
           statements.add(declaration());
       }

       return statements;
    }

    // a program is a number of declarations that happens at the same time
    private Statement declaration() {
        try {
            if (match(VAR)) {
                return varDeclaration();
            } else if (match(CLASS)) {
                return classDeclaration();
            } else if (match(FUN)) {
                return funDeclaration();
            } else {
                return statement(); // parsing will handle syntax error
            }
        } catch (ParseError error) {
           synchronize(); // jump out of current statement, so we can continue parsing and perhaps report more errors
           return null;
        }
    }

    private Statement classDeclaration() {
        Token name = consume(IDENTIFIER, "Expect class name.");

        consume(LEFT_BRACE, "Expect '{' before class body.");
        List<Statement.Function> methods = new ArrayList<>();
        while(!check(RIGHT_BRACE) && !isAtEnd()) {
            methods.add(funDeclaration());
        }
        consume(RIGHT_BRACE, "Expect '}' after class body.");

        return new Statement.Class(name, methods);
    }

    private Statement.Function funDeclaration() {
        Token name = consume(IDENTIFIER, "Expect function name.");

        consume(LEFT_PAREN, "Expect '(' after function name.");
        List<Token> parameters = new ArrayList<>();
        if (!check(RIGHT_PAREN)) {
            do {
                if (parameters.size() >= 255) {
                    Lox.error(peek(), "Cannot have more than 255 parameters.");
                }
                parameters.add(consume(IDENTIFIER, "Expect parameter name."));
            } while(match(COMMA));
        }
        consume(RIGHT_PAREN, "Expect ')' after parameters.");
        consume(LEFT_BRACE, "Expect '{' before function body.");

        return new Statement.Function(name, parameters, block());
    }

    private Statement varDeclaration() {
        Token name = consume(IDENTIFIER, "Expect variable name");
        Expression initializer = null;

        if (match(EQUAL)) initializer = expression();

        consume(SEMICOLON, "Expect ';' after variable declaration");
        return new Statement.Var(name, initializer);
    }

    private Statement statement() {
        if(match(WHILE)) {
            return whileStatement();
        } else if (match(FOR)) {
            return forStatement();
        } else if (match(LEFT_BRACE)) {
            return blockStatement();
        } else if (match(IF)) {
            return ifStatement();
        } else if (match(RETURN)) {
            return returnStatement();
        } else {
            return expressionStatement();
        }
    }

    private Statement returnStatement() {
        Token keyword = previous();
        Expression value = null;

        if (!check(SEMICOLON)) value = expression();

        consume(SEMICOLON, "Expect ';' after return statement");
        return new Statement.Return(keyword, value);
    }

    private Statement expressionStatement() {
        Expression expression = expression();
        consume(SEMICOLON, "Expect ';' after expression");
        return new Statement.Expr(expression);
    }

    private Statement ifStatement() {
        consume(LEFT_PAREN, "Expect '(' after 'if'.");
        Expression condition = expression();
        consume(RIGHT_PAREN, "Expect ')' after if condition.");

        Statement thenBranch = statement();
        Statement elseBranch = null;

        if (match(ELSE)) elseBranch = statement();

        return new Statement.If(condition, thenBranch, elseBranch);
    }

    private Statement forStatement() {
        consume(LEFT_PAREN, "Expect '(' after 'for'.");

        Statement initializer;
        if (match(SEMICOLON)) {
            initializer = null;
        } else if (match(VAR)) {
            initializer = varDeclaration();
        } else {
            initializer = expressionStatement();
        }

        Expression condition;
        if (!check(SEMICOLON)) {
            condition = expression();
        } else {
            condition = new Expression.Literal(true);
        }
        consume(SEMICOLON, "Expect ';' after loop condition");

        Expression increment;
        if (!check(RIGHT_PAREN)) {
            increment = expression();
        } else {
            increment = null;
        }
        consume(RIGHT_PAREN, "Expect ')' after loop clauses.");

        Statement body = statement();

        if (increment != null) {
            body = new Statement.Block(List.of(body, new Statement.Expr(increment)));
        }

        body = new Statement.While(condition, body);

        if (initializer != null) {
            body = new Statement.Block(List.of(initializer, body));
        }

        return body;
    }

    private Statement whileStatement() {
       consume(LEFT_PAREN, "Expect '(' after 'while'.");
       Expression condition = expression();
       consume(RIGHT_PAREN, "Expect ')' after condition.");

       Statement body = statement();

       return new Statement.While(condition, body);
    }

    private Statement blockStatement() {
        return new Statement.Block(block());
    }

    private List<Statement> block() {
        List<Statement> statements = new ArrayList<>();

        while (!check(RIGHT_BRACE) && !isAtEnd()) {
            statements.add(declaration());
        }

        consume(RIGHT_BRACE, "Expect '}' after block.");
        return statements;
    }

    private Expression expression() {
        return assignment();
    }

    private Expression assignment() {
        // assignments are expressions like in C but not Go
        Expression expression = logic_or();

        if (match(EQUAL)) { // unlike, binary we don't loop to potentially build a sequence of operators
            Token equals = previous();
            Expression value = assignment();

            // check if the lhs evaluated to a variable expression e.g new Point(3, 4).y evaluates to the field 'y'
            // which is a valid assignment target e.g new 'Point(3, 4).y = 5'
            if (expression instanceof Expression.Variable) {
                Token name = ((Expression.Variable) expression).name();
                return new Expression.Assignment(name, value); // convert r-value representation to l-value representation
            } else if (expression instanceof Expression.Get(Expression object, Token name)) {
                // anObject.property = X, property access (get) with assignment counts as a set-expression
                return new Expression.Set(object, name, value);
            }

            // lhs is not a variable, we don't need to panic just report error and keep parsing
            Lox.error(equals.line(), "Invalid assignment target.");
        }

       return expression; // wasn't an assignment
    }

    private Expression logic_or() {
        return logical(this::logic_and, OR);
    }

    private Expression logic_and(Void unused) {
        return logical(this::equality, AND);
    }

    private Expression logical(Function<Void, Expression> nextRule, Token.Type logicalOperator) {
        Expression expression = nextRule.apply(null);

        while (match(logicalOperator)) {
            Token operator = previous();
            Expression right = nextRule.apply(null);
            expression = new Expression.Logical(expression, operator, right);
        }

        return expression;
    }

    private Expression equality(Void unused) {
        return binary(this::comparison, BANG_EQUAL, EQUAL_EQUAL);
    }

    private Expression comparison(Void unused) {
        return binary(this::term, GREATER, GREATER_EQUAL, LESS, LESS_EQUAL);
    }

    private Expression term(Void unused) {
        return binary(this::factor, MINUS, PLUS);
    }

    private Expression factor(Void unused) {
        return binary(this::unary, FRONT_SLASH, STAR);
    }

    private Expression binary(Function<Void, Expression> nextRule, Token.Type... tokenTypes) {
        Expression expression = nextRule.apply(null);

        while (match(tokenTypes)) {
            Token operator = previous();
            Expression right = nextRule.apply(null);
            // this is the reason we don't treat logical as a regular binary,
            // the visitor code would have to treat 'and', 'or' as a special case
            expression = new Expression.Binary(expression, operator, right);
        }

        return expression;
    }

    private Expression unary(Void unused) {
        if (match(BANG, MINUS)) {
            Token operator = previous();
            Expression right = unary(null);
            return new Expression.Unary(operator, right);
        }

        return call();
    }

    private Expression call() {
        Expression expression = primary();

        while (true) {
            if (match(LEFT_PAREN)) {
                expression = finishCall(expression);
            } else if (match(DOT)) {
                Token name = consume(IDENTIFIER, "Expect property name after '.'.");
                expression = new Expression.Get(expression, name);
            } else {
                break;
            }
        }

        return expression;
    }

    private Expression finishCall(Expression callee) {
        List<Expression> arguments = new ArrayList<>();

        if (!check(RIGHT_PAREN)) {
            do {
                if (arguments.size() >= 255) {
                    Lox.error(peek(), "Cannot have more than 255 arguments.");
                    throw new ParseError();
                }
                arguments.add(expression());
            } while (match(COMMA));
        }

        Token rightParen = consume(RIGHT_PAREN, "Expect ')' after arguments.");

        return new Expression.Call(callee, rightParen, arguments);
    }

    private Expression primary() {
        if (match(TRUE)) return new Expression.Literal(true);
        if (match(FALSE)) return new Expression.Literal(false);
        if (match(NIL)) return new Expression.Literal(null);
        if (match(NUMBER, STRING)) return new Expression.Literal(previous().literal());
        if (match(THIS)) return new Expression.This(previous());
        if (match(IDENTIFIER)) return new Expression.Variable(previous());

        if (match(LEFT_PAREN)) {
            Expression expression = expression();
            consume(RIGHT_PAREN, "Expect ')' after expression.");
            return new Expression.Grouping(expression);
        }

        Lox.error(peek(), "Expect expression.");
        throw new ParseError();
    }

    private Token consume(Token.Type tokenType, String errorMessage) {
        if (check(tokenType)) {
            return advance();
        } else {
            Lox.error(peek(), errorMessage);
            throw new ParseError();
        }
    }

    private void synchronize() {
        advance();

        while (!isAtEnd()) {
            if (previous().type() == SEMICOLON) return; // end of statement that caused panic

            // beginning of a new statement
            switch (peek().type()) {
                case CLASS:
                case FOR:
                case FUN:
                case IF:
                case RETURN:
                case VAR:
                case WHILE:
                    return;
            }

            advance();
        }
    }

    // this is the implementation of single token lookahead
    private boolean match(Token.Type... types) {
        for (Token.Type type : types) {
            if (check(type)) {
                advance();
                return true;
            }
        }
        return false;
    }

    private boolean check(Token.Type type) {
        if (isAtEnd()) return false;
        return peek().type() == type;
    }

    private Token advance() {
        if (!isAtEnd()) ++current;
        return previous();
    }

    private Token peek() {
        return tokens.get(current);
    }

    // the last consumed token
    private Token previous() {
        return tokens.get(current -1);
    }

    private boolean isAtEnd() {
        return peek().type() == EOF;
    }
}
