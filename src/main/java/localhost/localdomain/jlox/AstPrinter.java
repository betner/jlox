package localhost.localdomain.jlox;

import java.util.List;
import java.util.stream.Collectors;

class AstPrinter implements Expression.Visitor<String>, Statement.Visitor<String> {
    @Override
    public String visit(Expression.Binary binary) {
        return parenthesize(binary.operator().lexeme(), binary.left(), binary.right());
    }

    @Override
    public String visit(Expression.Unary unary) {
        return parenthesize(unary.operator().lexeme(), unary.right());
    }

    @Override
    public String visit(Expression.Grouping grouping) {
        return parenthesize("group", grouping.expression());
    }

    @Override
    public String visit(Expression.Literal literal) {
        if (literal.value() == null) return "nil";
        return literal.value().toString();
    }

    @Override
    public String visit(Expression.Variable variable) {
        return variable.name().lexeme();
    }

    @Override
    public String visit(Expression.Assignment assignment) {
        return parenthesize("assignment", assignment.value());
    }

    @Override
    public String visit(Expression.Logical logical) {
        return parenthesize(logical.operator().lexeme(), logical.left(), logical.right());
    }

    @Override
    public String visit(Expression.Call call) {
        return parenthesize("call " + call.callee().accept(this), call.arguments().toArray(new Expression[0]));
    }

    @Override
    public String visit(Expression.Get get) {
        return "(get " + get.name() + ")";
    }

    @Override
    public String visit(Expression.Set set) {
        return parenthesize("set "
                + set.object().accept(this)
                +" "
                + set.name().lexeme(),
                set.value());
    }

    @Override
    public String visit(Expression.This expr) {
        return "this";
    }

    @Override
    public String visit(Statement.Class classDeclaration) {
        return parenthesize("class "
                + classDeclaration.name().lexeme()
                + " ",
                classDeclaration.methods().toArray(new Statement[0]));
    }

    @Override
    public String visit(Statement.Function funDeclaration) {
        String params = funDeclaration.parameters().stream().map(Token::lexeme).collect(Collectors.joining(", "));
        return parenthesize("fun "
                + funDeclaration.name().lexeme()
                + " "
                + params,
                funDeclaration.body().toArray(new Statement[0]));
    }

    @Override
    public String visit(Statement.Var varDeclaration) {
        Expression initializer = new Expression.Literal("nil");
        if (varDeclaration.initializer() != null) initializer = varDeclaration.initializer();

        return parenthesize("var " + varDeclaration.name().lexeme(), initializer);
    }

    @Override
    public String visit(Statement.Expr statement) {
        return parenthesize("statement", statement.expression());
    }

    @Override
    public String visit(Statement.While whileStmt) {
        return parenthesize("while" + parenthesize("condition", whileStmt.condition()), whileStmt.body());
    }

    @Override
    public String visit(Statement.Block block) {
        return parenthesize("block", block.statements().toArray(new Statement[0]));
    }

    @Override
    public String visit(Statement.Return returnStmt) {
        return parenthesize(returnStmt.keyword().lexeme(), returnStmt.value());
    }

    @Override
    public String visit(Statement.If ifStatement) {
        if (ifStatement.elseBranch() == null) {
            return parenthesize("if" + parenthesize("condition", ifStatement.condition()), ifStatement.thenBranch());
        }
        return parenthesize("if" + parenthesize("condition", ifStatement.condition()), ifStatement.thenBranch(), ifStatement.elseBranch());
    }

    public String print(List<Statement> statements) {
        return statements.stream().map(s -> s.accept(this)).collect(Collectors.joining("\n"));
    }

    private String parenthesize(String name, Statement... statements) {
        StringBuilder builder = new StringBuilder();

        builder.append("(").append(name);
        for (Statement stmt : statements) {
            builder.append(" ");
            builder.append(stmt.accept(this));
        }
        builder.append(")");

        return builder.toString();
    }

    private String parenthesize(String name, Expression... expressions) {
        StringBuilder builder = new StringBuilder();

        builder.append("(").append(name);
        for (Expression expr : expressions) {
            builder.append(" ");
            builder.append(expr.accept(this));
        }
        builder.append(")");

        return builder.toString();
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner("""
            34 + 33;
            
            var foo = 2;
            var bar;
            var baz = nil;
            
            foo;
            
            if (2==2 and 2!=3) {
                print("true");
            } else {
                print("false");
            }
            
            while (1==1) { print("loop forever"); }
            
            fun aFunction(p1, p2) { return p1 + p2; }
            
            class OneLoxClass {
                aMethod(a, b) {
                    print(this);
                    return a + b;
                }
            }
            
            var instance = OneLoxClass();
            
            instance.aField = 1;
            """
        );

        Parser parser = new Parser(scanner.tokens());
        var statements = parser.parse();

        System.out.println(new AstPrinter().print(statements));
    }
}
