package localhost.localdomain.jlox;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Interpreter implements Expression.Visitor<Object>, Statement.Visitor<Void> {
    private final Map<Expression, Integer> locals = new HashMap<>();
    private final Environment globals = new Environment();
    private Environment environment = globals; // this reference changes in scopes

    Interpreter() {
        globals.define("clock", (LoxCallable) (interpreter, args) -> (double) System.currentTimeMillis() / 1000.0);
        globals.define("print", new LoxCallable() {
            @Override
            public Object call(Interpreter interpreter, List<Object> args) {
                System.out.println(args.getFirst());
                return null;
            }
            @Override
            public int arity() {return 1;}
        });
    }

    void resolve(Expression expression, int depth) {
        locals.put(expression, depth);
    }

    Environment globals() {
        return globals;
    }

    @Override
    public Void visit(Statement.Class statement) {
        environment.define(statement.name().lexeme(),  null);
        HashMap<String, LoxFunction> methods = new HashMap<>();

        for (Statement.Function sf: statement.methods()) {
            var function = new LoxFunction(sf, environment, sf.name().lexeme().equals("init"));
            methods.put(sf.name().lexeme(), function);
        }

        LoxClass loxClass = new LoxClass(statement.name().lexeme(), methods);
        environment.assign(statement.name(), loxClass);
        return null;
    }

    @Override
    public Void visit(Statement.Function statement) {
        LoxFunction function = new LoxFunction(statement, environment, false);
        environment.define(statement.name().lexeme(), function);
        return null;
    }

    @Override
    public Void visit(Statement.Var statement) {
        Object value = null;

        if (statement.initializer() != null) {
            value = evaluate(statement.initializer());
        }

        environment.define(statement.name().lexeme(), value);
        return null;
    }

    @Override
    public Void visit(Statement.Expr statement) {
        evaluate(statement.expression());
        return null;
    }

    @Override
    public Void visit(Statement.If statement) {
        if (isTruthy(evaluate(statement.condition()))) {
            execute(statement.thenBranch());
        } else if(statement.elseBranch() != null) {
            execute(statement.elseBranch());
        }
        return null;
    }

    @Override
    public Void visit(Statement.While statement) {
        while (isTruthy(evaluate(statement.condition()))) {
            execute(statement.body());
        }
        return null;
    }

    @Override
    public Void visit(Statement.Block statement) {
        execute(statement.statements(), new Environment(environment));
        return null;
    }

    @Override
    public Void visit(Statement.Return statement) {
        Object value = null;
        if (statement.value() != null) value = evaluate(statement.value());

        throw new Return(value);
    }

    @Override
    public Object visit(Expression.Binary binary) {
        Object left = evaluate(binary.left());
        Object right = evaluate(binary.right());
        Token operator = binary.operator();

        return switch (operator.type()) {
            case PLUS -> addOrConcatenate(operator, left, right);
            case FRONT_SLASH -> {
                checkNumberOperands(operator, left, right);
                yield (double) left / (double) right;
            }
            case MINUS -> {
                checkNumberOperands(operator, left, right);
                yield (double) left - (double) right;
            }
            case STAR -> {
                checkNumberOperands(operator, left, right);
                yield (double) left * (double) right;
            }
            case GREATER -> {
                checkNumberOperands(operator, left, right);
                yield (double) left > (double) right;
            }
            case GREATER_EQUAL -> {
                checkNumberOperands(operator, left, right);
                yield (double) left >= (double) right;
            }
            case LESS -> {
                checkNumberOperands(operator, left, right);
                yield (double) left < (double) right;
            }
            case LESS_EQUAL -> {
                checkNumberOperands(operator, left, right);
                yield (double) left <= (double) right;
            }
            case EQUAL_EQUAL -> {
                checkNumberOperands(operator, left, right);
                yield isEqual(left, right);
            }
            case BANG_EQUAL -> {
                checkNumberOperands(operator, left, right);
                yield !isEqual(left, right);
            }
            default -> throw new RuntimeError(operator, "Unknown binary operator");
        };
    }

    @Override
    public Object visit(Expression.Unary unary) {
        Object right = evaluate(unary.right());

        return switch (unary.operator().type()) {
            case MINUS -> {
                checkNumberOperands(unary.operator(), right);
                yield -(double) right;
            }
            case BANG -> !isTruthy(right);
            default -> throw new RuntimeError(unary.operator(), "Unknown unary operator");
        };
    }

    @Override
    public Object visit(Expression.Grouping grouping) {
        return evaluate(grouping.expression());
    }

    @Override
    public Object visit(Expression.Literal literal) {
        return literal.value();
    }

    @Override
    public Object visit(Expression.Variable variable) {
        return lookUpVariable(variable.name(), variable);
    }

    @Override
    public Object visit(Expression.Assignment assignment) {
        Object value = evaluate(assignment.value());

        Integer distance = locals.get(assignment);
        if (distance != null) {
            environment.assignAt(distance, assignment.name(), value);
        } else {
            globals.assign(assignment.name(), value);
        }

        return value;
    }

    @Override
    public Object visit(Expression.Logical logical) {
        Object left = evaluate(logical.left());

        if (logical.operator().type() == Token.Type.OR) {
            if (isTruthy(left)) return left;
        } else {
            if (!isTruthy(left)) return left;
        }

        return evaluate(logical.right()); // true AND true, false OR true
    }

    @Override
    public Object visit(Expression.Call call) {
        Object callee = evaluate(call.callee());

        if (!(callee instanceof LoxCallable function)) {
            throw new RuntimeError(call.rightParen(), "Can only call functions and classes.");
        }

        List<Object> evaluatedArgs = new ArrayList<>();
        for (Expression expr: call.arguments()) {
            evaluatedArgs.add(evaluate(expr));
        }

        if (function.arity() != evaluatedArgs.size()) {
            throw new RuntimeError(call.rightParen(), "Expected "
                    + function.arity()
                    + " arguments but got "
                    + evaluatedArgs.size()
                    + ".");
        }

        return function.call(this, evaluatedArgs);
    }

    @Override
    public Object visit(Expression.Get get) {
        Object object = evaluate(get.object());

        if (object instanceof LoxInstance) {
            return ((LoxInstance) object).get(get.name());
        }

        throw new RuntimeError(get.name(), "Only objects have properties.");
    }

    @Override
    public Object visit(Expression.Set set) {
        Object object = evaluate(set.object());

        if (object instanceof LoxInstance) {
            Object value = evaluate(set.value());
            ((LoxInstance) object).set(set.name(), value);
            return value;
        }

        throw new RuntimeError(set.name(), "Only objects have fields.");
    }

    @Override
    public Object visit(Expression.This expr) {
        return lookUpVariable(expr.keyword(), expr);
    }

    void interpret(List<Statement> statements) {
        try {
            statements.forEach(this::execute);
        } catch (RuntimeError error) {
            Lox.runtimeError(error);
        }
    }

    void execute(List<Statement> statements, Environment current) {
       Environment previous = this.environment;
       try {
           this.environment = current;

           for (Statement s : statements) {
               execute(s);
           }
       } finally { // returns are implemented as RuntimeExceptions, so we need this to restore the environment after a return  statement
           this.environment = previous;
       }
    }

    private void execute(Statement statement) {
        statement.accept(this);
    }

    private Object evaluate(Expression expression) {
        return expression.accept(this);
    }

    private String stringify(Object object) {
        if (object == null) return "nil";

        if (object instanceof Double) {
           String text = object.toString();
           if (text.endsWith(".0")) {
               text = text.substring(0, text.length() - 2);
           }
           return text;
        }

        return object.toString();
    }

    private void checkNumberOperands(Token operator, Object... operands) {
        for (Object op: operands) {
            if (!(op instanceof Double))
                throw new RuntimeError(operator, "Operands must be numbers");
        }
    }

    private boolean isTruthy(Object object) {
        if (object == null) return false;
        if (object instanceof Boolean) return (boolean) object;
        return true;
    }

    private boolean isEqual(Object a, Object b) {
        // compare null values, don't throw NPE
        if (a == null && b == null) return true;
        if (a == null) return false;

        return a.equals(b);
    }

    private Object addOrConcatenate(Token operator, Object left, Object right) {
        return switch (left) {
            case Double number when right instanceof Double -> number + (double) right;
            case Double number when right instanceof String -> stringify(number) + right;
            case String string when right instanceof Double -> string + stringify(right);
            case String string when right instanceof String -> string + right;
            case null, default -> throw new RuntimeError(operator, "Operands must be numbers or strings.");
        };
    }

    private Object lookUpVariable(Token name, Expression expression) {
        Integer distance = locals.get(expression);
        if (distance != null) {
            return environment.getAt(distance, name.lexeme());
        } else {
            return globals.get(name);
        }
    }
}
