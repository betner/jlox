package localhost.localdomain.jlox;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;


public class Lox {

    private static boolean hadError;
    private static boolean hadRuntimeError;

    public static void main(String[] args) throws IOException {
        if (args.length > 1) {
            System.out.println("Usage: jlox [script]");
            System.exit(Sysexits.EX_USAGE.code());
        } else if (args.length == 1) {
            runFile(args[0]);
        } else {
            runPrompt();
        }
    }

    // used by the lexical analyser to report a parse error
    static void error(int line, String message) {
        hadError = true;
        System.err.println("[line " + line + "] Error : " + message);
    }

    static void error(Token token, String message) {
        hadError = true;
        if (token.type() == Token.Type.EOF) {
           report(token.line(), " at end", message);
        } else {
            report(token.line(), " at '" + token.lexeme() + "'", message);
        }
    }

    static void runtimeError(RuntimeError error) {
        System.err.println(error.getMessage() + "\n[line " + error.token.line() + "]");
        hadRuntimeError = true;
    }

    private static void report(int line, String where, String message) {
        System.err.println("[line " + line + "] Error" + where + ": " + message);
    }

    private static void runFile(String path) throws IOException {
        byte[] bytes = Files.readAllBytes(Paths.get(path));
        run(new String(bytes, Charset.defaultCharset()));
        if (hadError) System.exit(Sysexits.EX_DATAERR.code());
        if (hadRuntimeError) System.exit(Sysexits.EX_SOFTWARE.code());
    }

    private static void runPrompt() throws IOException {
        InputStreamReader input = new InputStreamReader(System.in);
        BufferedReader reader = new BufferedReader(input);

        while(true) {
            System.out.print("> ");
            String line = reader.readLine();
            if (line == null) break;
            run(line);
            hadError = false; // allow REPL to continue
        }
    }

    private static void run(String source) {
        Scanner scanner = new Scanner(source);
        Parser parser = new Parser(scanner.tokens());
        var statements = parser.parse();

        if (hadError) return;

        Interpreter interpreter = new Interpreter();
        Resolver resolver = new Resolver(interpreter);
        resolver.run(statements);

        if (hadError) return;

        interpreter.interpret(statements);
       // System.out.println(new AstPrinter().print(expression));

    }
}
