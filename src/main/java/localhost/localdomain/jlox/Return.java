package localhost.localdomain.jlox;

public class Return extends RuntimeException {
    final Object value;

    public Return(Object value) {
        super(null, null, false, false); // do not treat this as a "normal" exception
        this.value = value;
    }
}
