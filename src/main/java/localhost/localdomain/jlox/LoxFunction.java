package localhost.localdomain.jlox;

import java.util.List;

public record LoxFunction(Statement.Function declaration,
                          Environment closure,
                          boolean isInitializer) implements LoxCallable {

    @Override
    public Object call(Interpreter interpreter, List<Object> arguments) {
        Environment environment = new Environment(closure);

        for (int i = 0; i < declaration.parameters().size(); i++) {
            environment.define(declaration.parameters().get(i).lexeme(), arguments.get(i));
        }

        try {
            interpreter.execute(declaration.body(), environment);
        } catch (Return returnValue) {
            if (isInitializer) {
                // an empty return statement would return 'nil' but
                // we have to return a reference to this instance
                return closure.getAt(0, "this");
            } else {
                return returnValue.value;
            }
        }

        // the "init" method has been called explicitly on an instance
        // so the "this" reference should be returned implicitly
        if (isInitializer) return closure.getAt(0, "this");

        return null;
    }

    @Override
    public int arity() {
        return declaration.parameters().size();
    }

    @Override
    public String toString() {
        return "<fn " + declaration.name() + ">";
    }

    LoxFunction bind(LoxInstance loxInstance) {
        var env = new Environment(closure);
        env.define("this", loxInstance);
        return new LoxFunction(declaration, env, isInitializer);
    }
}
