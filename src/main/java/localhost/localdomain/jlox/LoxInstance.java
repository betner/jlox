package localhost.localdomain.jlox;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class LoxInstance {
    private final LoxClass loxClass;
    private final Map<String, Object> fields = new HashMap<>();
    private final UUID id = UUID.randomUUID();

    public LoxInstance(LoxClass loxClass) {
        this.loxClass = loxClass;
    }

    public Object get(Token name) {
        String lexeme = name.lexeme();
        if (fields.containsKey(lexeme)) {
            return fields.get(lexeme);
        }

        // bind this Java instance to the "this" variable in the method's closure
        var method = loxClass.findMethod(name.lexeme());
        if (method != null) return method.bind(this);

        throw new RuntimeError(name, "Undefined property '" + lexeme + "'.");
    }

    public void set(Token name, Object value) {
        fields.put(name.lexeme(), value);
    }

    @Override
    public String toString() {
        return "<object " + loxClass.name + " '" + id + "'>";
    }
}
