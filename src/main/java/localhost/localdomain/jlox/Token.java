package localhost.localdomain.jlox;

public record Token(Type type,
                    String lexeme,
                    Object literal,
                    int line) {
    public enum Type {
        // Single character tokens
        LEFT_PAREN, RIGHT_PAREN, LEFT_BRACE, RIGHT_BRACE,
        COMMA, DOT, MINUS, PLUS, SEMICOLON, FRONT_SLASH, STAR,

        // One or two character tokens
        BANG, BANG_EQUAL,
        EQUAL, EQUAL_EQUAL,
        GREATER, GREATER_EQUAL,
        LESS, LESS_EQUAL,

        // Literals
        IDENTIFIER, STRING, NUMBER,

        // Keywords
        AND, CLASS, ELSE, FALSE, FUN, FOR, IF, NIL, OR,
        RETURN, SUPER, THIS, TRUE, VAR, WHILE,

        EOF
    }
}

// TODO: kolumn och rad kan räknas ut om vi sparar offset för denna token
// alltså vilken position den börjar på relaterat början av filen
// så vi kan beräkna detta dynamiskt i klassen, spara det som fält eller
// lägga till int col i ctor