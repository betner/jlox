package localhost.localdomain.jlox;

import java.util.List;

interface LoxCallable {
    Object call(Interpreter interpreter, List<Object> arguments);
    default int arity() {
        return 0;
    }
}
