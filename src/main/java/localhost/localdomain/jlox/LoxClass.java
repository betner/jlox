package localhost.localdomain.jlox;

import java.util.HashMap;
import java.util.List;

public class LoxClass implements LoxCallable {
    final String name;
    final HashMap<String, LoxFunction> methods;

    public LoxClass(String name, HashMap<String, LoxFunction> methods) {
        this.name = name;
        this.methods = methods;
    }

    @Override
    public Object call(Interpreter interpreter, List<Object> arguments) {
        var instance = new LoxInstance(this);
        var initializer = findMethod("init");

        if (initializer != null) {
            initializer.bind(instance).call(interpreter, arguments);
        }

        return instance;
    }

    @Override
    public int arity() {
        var initializer = findMethod("init");

        if (initializer == null) return 0;

        return initializer.arity();
    }

    @Override
    public String toString() {
        return name;
    }

    public LoxFunction findMethod(String name) {
        return methods.getOrDefault(name, null);
    }
}
