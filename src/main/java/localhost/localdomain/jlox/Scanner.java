package localhost.localdomain.jlox;

import java.util.*;

import static localhost.localdomain.jlox.Token.Type.*;

public class Scanner {

    private final String source;
    private final ArrayList<Token> tokens;
    private final Map<String, Token.Type> keywords;
    private int current;
    private int line;
    private int start;

    Scanner(String source) {
        this.source = source;
        this.tokens = new ArrayList<>();
        this.keywords = initKeywords();
        this.current = 0;
        this.line = 1;
        this.start = 0;
    }

    List<Token> tokens() {
        while (!atEnd()) {
            start = current;
            scanToken();
        }
        tokens.add(new Token(EOF, "", null, line));
        return tokens;
    }

    private void scanToken() {
        char c = advance();
        switch (Character.valueOf(c)) {
            case '(' -> addToken(LEFT_PAREN);
            case ')' -> addToken(RIGHT_PAREN);
            case '{' -> addToken(LEFT_BRACE);
            case '}' -> addToken(RIGHT_BRACE);
            case ',' -> addToken(COMMA);
            case '.' -> addToken(DOT);
            case '-' -> addToken(MINUS);
            case '+' -> addToken(PLUS);
            case ';' -> addToken(SEMICOLON);
            case '*' -> addToken(STAR);
            case '!' -> addToken(match('=') ? BANG_EQUAL : BANG);
            case '=' -> addToken(match('=') ? EQUAL_EQUAL : EQUAL);
            case '>' -> addToken(match('=') ? GREATER_EQUAL : GREATER);
            case '<' -> addToken(match('=') ? LESS_EQUAL : LESS);
            case '"' -> string();
            case Character d when isDigit(d) -> number();
            case Character a when isAlpha(a) -> identifier();
            case '/' -> {
                if (match('/')) {
                    lineComment();
                } else if (match('*')) {
                    blockComment();
                } else {
                    addToken(FRONT_SLASH);
                }
            }
            case '\n' -> line++;
            case ' ', '\t', '\r' -> {}
            default -> Lox.error(line, "Unexpected character '" + c +  "'");
        }
    }

    private boolean match(char expected) {
        if (atEnd()) return false;
        if (source.charAt(current) != expected) return false;

        current++;
        return true;
    }


    // Lox supports multiline strings
    private void string() {
        while (peek() != '"' && !atEnd()) {
            if (peek() == '\n') line++;
            advance();
        }

        // the loop has finished, either we are at the end or we have a closed string

        if (atEnd()) {
            Lox.error(line, "Unterminated string");
            return;
        }

        // eat the last "
        advance();

        var literal = source.substring(start + 1, current -1);
        addToken(STRING, literal);
    }

    // allows 242 0.1 1.0 11.00 but not .1 1.
    private void number() {
       while (isDigit(peek())) advance();

       // look for fractional part
       if (peek() == '.' && isDigit(peekNext())) {
           do advance();
           while (isDigit(peek()));
       }

       var number = source.substring(start, current);
       addToken(NUMBER, Double.parseDouble(number));
    }

    private boolean isDigit(char d) {
        return d >= '0' && d <= '9';
    }

    private void identifier() {
        while(isAlphaNumberic(peek())) advance();

        var text = source.substring(start, current);
        var tokenType = keywords.get(text);
        if (tokenType == null) tokenType = IDENTIFIER;

        addToken(tokenType, text);
    }

    private void lineComment() {
        // don't consume '\n', it's needed to increase line count
        while (peek() != '\n' && !atEnd()) advance();
    }

    private void blockComment() {
        while (!(peek() == '*' && peekNext() == '/') && !atEnd()) {
            if (peek() == '\n') line++;
            advance();
        }

        if (atEnd()) {
            Lox.error(line, "Block comment not terminated");
        } else {
            // eat '*/'
            advance();
            advance();
        }
    }

    private boolean isAlphaNumberic(char c) {
        return isAlpha(c) || isDigit(c);
    }

    private boolean isAlpha(char c) {
        return (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') || c == '_';
    }

    private char advance() {
        return source.charAt(current++);
    }

    private char peek() {
        if (atEnd()) return '\0';

        return source.charAt(current);
    }

    private char peekNext() {
        if (current + 1 >= source.length()) return '\0';
        return source.charAt(current + 1);
    }

    private void addToken(Token.Type tokenType) {
        addToken(tokenType, null);
    }

    private void addToken(Token.Type tokenType, Object literal) {
        String text = source.substring(start, current);
        tokens.add(new Token(tokenType, text, literal, line));
    }

    private boolean atEnd() {
        return current >= source.length();
    }

    private Map<String, Token.Type> initKeywords() {
        var map = new HashMap<String, Token.Type>();
        map.put("and",    AND);
        map.put("class",  CLASS);
        map.put("else",   ELSE);
        map.put("false",  FALSE);
        map.put("for",    FOR);
        map.put("fun",    FUN);
        map.put("if",     IF);
        map.put("nil",    NIL);
        map.put("or",     OR);
        map.put("return", RETURN);
        map.put("super",  SUPER);
        map.put("this",   THIS);
        map.put("true",   TRUE);
        map.put("var",    VAR);
        map.put("while",  WHILE);

        return map;
    }
}
